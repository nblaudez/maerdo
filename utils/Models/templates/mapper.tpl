<?php echo "<?php\n"?>

class <?php echo $params['module']?>_Model_Mappers_<?php echo $params['filename']?> extends <?php echo $params['module']?>_Model_Mappers_Abstract {

    public function getDbTable()  {
       if (null === $this->_dbTable) {
            $this->setDbTable('<?php echo ucfirst($params['module'])?>_Model_Dbtable_<?php echo ucfirst($params['filename'])?>');
       }
       return $this->_dbTable;
    }
    
	/**
     * fetches all rows 
     *
     * @return array
     */
    public function fetchAll()  {
    	if(My_Class_Maerdo_Cache::test('DB_<?php echo $params['module']?>_<?php echo $params['filename']?>_fetchAll')) {
    		$resultSet=My_Class_Maerdo_Cache::get('DB_<?php echo $params['module']?>_<?php echo $params['filename']?>_fetchAll');
    	} else {
        	$resultSet = $this->getDbTable()->fetchAll();
        	My_Class_Maerdo_Cache::set('DB_<?php echo $params['module']?>_<?php echo $params['filename']?>_fetchAll',$resultSet,0,array('sqlSelect'));
        }	
        
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new <?php echo $params['module']?>_Model_<?php echo $params['filename']?>();
            $entry<?php $count=count($params['fields']);foreach ($params['fields'] as $column): $count--?>->set<?php echo ucfirst($column)?>($row-><?php echo $column?>)
                  <?php endforeach;?>
            ->setMapper($this);
            $entries[] = $entry;
        }
        return $entries;
    }


	/**
     * saves current row
     *
     * @param <?php echo $params['module']?>_Model_<?php echo $params['table']?> $model
     *
     */
     
    public function save(<?php echo $params['module']?>_Model_<?php echo $params['filename']?> $model,$ignoreEmptyValuesOnUpdate=true) {
        if ($ignoreEmptyValuesOnUpdate) {
            $data = $model->toArray();
            foreach ($data as $key=>$value) {
                if (is_null($value) or $value == '')
                    unset($data[$key]);
            }
        }

        if (null === ($id = $model->get<?php echo ucfirst($params['primarykey'])?>())) {
            unset($data['<?php echo $params['primarykey']?>']);
            $id=$this->getDbTable()->insert($data);
            $model->set<?php echo ucfirst($params['primarykey'])?>($id);
            return($id);
        } else {
            if ($ignoreEmptyValuesOnUpdate) {
             $data = $model->toArray();
             foreach ($data as $key=>$value) {
                if (is_null($value) or $value == '')
                    unset($data[$key]);
                }
            }

            return($this->getDbTable()->update($data, array('<?php echo $params['primarykey']?> = ?' => $id)));
        }
    }
   
    /**
     * returns an array, keys are the field names.
     *
     * @param new <?php echo $params['module']?>_Model_<?php echo $params['table']?> $model
     * @return array
     *
     */
    public function toArray($model) {
        $result = array(

            <?php foreach ($params['fields'] as $column):?>'<?php echo $column?>' => $model->get<?php echo ucfirst($column)?>(),
            <?php endforeach;?>
        
        );
        return $result;
    }   
    
    /**
     * finds rows where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param <?php echo $params['module']?>_Model_<?php echo $params['filename']?> $model
     * @return array
     */
    public function findByField($field, $value, $model)  {            
            $select = $this->getDbTable()->select();
            $select->where("{$field} = ?", $value);
           
			$rows=My_Class_Maerdo_Cache::getQueryResult($select);
			
			$result = array();           
            foreach ($rows as $row) {
                    $model=new <?php echo $params['module']?>_Model_<?php echo $params['filename']?>();
                    $result[]=$model;
                    $model<?php $count=count($params['fields']); foreach ($params['fields'] as $column): $count--?>->set<?php echo ucfirst($column)?>($row['<?php echo $column?>'])<?php if ($count> 0) echo "\n\t\t\t\t\t\t  "; endforeach;?>;
            }
            return $result;
    }    

   /**
     * find row by id
     *
     * @param string $id
     */
    public function find($id) {
            $table = $this->getDbTable();
            
	    	if(My_Class_Maerdo_Cache::test('DB_<?php echo $params['module']?>_<?php echo $params['filename']?>_find_'.$id)) {
	    		$row=My_Class_Maerdo_Cache::get('DB_<?php echo $params['module']?>_<?php echo $params['filename']?>_find_'.$id);
	    	} else {
	        	$row = $table->find($id);
	        	//My_Class_Maerdo_Cache::set('DB_<?php echo $params['module']?>_<?php echo $params['filename']?>_find_'.$id,$row,1,array('sqlSelect'));
	        }	
                    
   			foreach ($row as $data) {
           		$model=new <?php echo $params['module']?>_Model_<?php echo $params['filename']?>();
            	$model<?php $count=count($params['fields']); foreach ($params['fields'] as $column): $count--?>->set<?php echo ucfirst($column)?>($data['<?php echo $column?>'])<?php if ($count> 0) echo "\n\t\t\t\t\t  "; endforeach;?>;
			}
            return ($model);
    } 
    
    
    public function deleteByPrimarykey($value) {
        return $this->getDbTable()->delete('<?php echo $params['primarykey']?> = '.$value);
    }    
}
